# ======================================================================
# Copyright TOTAL / CERFACS / LIRMM (09/2020)
# Contributor: Adrien Suau (<adrien.suau@cerfacs.fr>
#                           <adrien.suau@lirmm.fr>)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================
from qaths.applications.wave_equation.evolve_1D_dirichlet import evolve_1d_dirichlet
from qaths.applications.wave_equation.linking_sets.arithmetic_adder import (
    get_linking_set as arith_linking_set,
)
from qprof import profile


def main():
    time = 0.1
    discretisation_size = 2 ** 10
    epsilon = 1e-3
    trotter_order = 1

    routine = evolve_1d_dirichlet(time, discretisation_size, epsilon, trotter_order)

    G = {"u1": 0, "u2": 89, "u3": 178, "cx": 930}
    gate_times = {
        "cu1": 2 * G["cx"] + 2 * G["u1"],
        "cu2": 2 * G["cx"] + 2 * G["u3"],
        "X": G["u3"],
        "H": G["u2"],
        "CNOT": G["cx"],
        "CCNOT": 6 * G["cx"] + 2 * G["u2"] + 7 * G["u1"],
        "CH": 2 * G["cx"] + 2 * G["u3"],
        "PH": 3 * G["u1"] + 2 * G["cx"],
        "CPH": 3 * G["u1"] + 2 * G["cx"],
        "CCPH": None,
    }
    gate_times["CCPH"] = 3 * gate_times["CPH"] + 2 * gate_times["CCNOT"]
    gate_times["CCCNOT"] = (
        6 * gate_times["CCNOT"] + 2 * gate_times["cu2"] + 7 * gate_times["cu1"]
    )

    result = profile(
        routine,
        gate_times,
        linking_set=arith_linking_set(discretisation_size),
        second_scale=1,
    )
    print(result)
