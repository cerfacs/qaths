# ======================================================================
# Copyright TOTAL / CERFACS / LIRMM (03/2020)
# Contributor: Adrien Suau (<adrien.suau@cerfacs.fr>
#                           <adrien.suau@lirmm.fr>)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================
from qaths._cli.data_generation.utils.ranges import LinearIntRange, PointRange
from qaths._cli.data_generation.utils.circuit_analysers import get_statistics
from qaths.applications.wave_equation.linking_sets.basic import get_linking_set
from qaths._cli.data_generation.utils.parallel import execute_task_in_parallel


def get_stats_from_tuple(tup):
    discretisation_point_number, t, eps, trotter = tup
    return get_statistics(
        *tup,
        linked_implementations=get_linking_set(discretisation_point_number),
        is_wave_equation_solver=False,
    )


def main():
    # Define the set of parameters we want to compute the data for.
    # Here we set the time, precision and trotter_order as constants and let the
    # discretisation size vary on a log range.
    discretisation = LinearIntRange(2 ** 2, 2 ** 100, 20)
    time = PointRange(1)
    epsilon = PointRange(1e-5)
    trotter_order = PointRange(1)

    # Compute the data on the parameters defined above.
    results = execute_task_in_parallel(
        get_stats_from_tuple, [discretisation, time, epsilon, trotter_order]
    )

    # Print the data on stdout, formatted as CSV.
    print("discretisation_points_number,time,epsilon,trotter_order,execution_time_ns")
    for res in results:
        discr, t, eps, trot = res["params"]
        exec_time = res["Melbourne_execution_time_ns"]
        print(f"{discr},{t},{eps},{trot},{exec_time}")


if __name__ == "__main__":
    main()
