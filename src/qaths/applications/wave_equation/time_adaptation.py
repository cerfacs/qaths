# ======================================================================
# Copyright CERFACS (June 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

r"""Module dedicated to the adaptation of the evolution time to solve the wave equation.

As described in the paper :cite:`1711.05394v1`, in order to solve the wave equation for
a time :math:`t`, a specific Hamiltonian should be simulated for a time :math:`f(t)`.
This module is dedicated to the computation of :math:`f(t)`.
"""


def adapt_dirichlet_evolution_time(
    pde_evolution_time: float, discretisation_point_number: int
) -> float:
    """Adapt the PDE evolution time to return the Hamiltonian Simulation evolution time

    :param pde_evolution_time: The time for which we want to solve the PDE.
    :param discretisation_point_number: The number of discretisation points used.
    :return: the evolution time required to solve the wave equation.
    """
    # We should simulate 1/a * H for a time t. The 1/a factor can be pushed in
    # the simulation time, which left us with the problem of simulating H for a
    # time t/a. See the remark in arXiv:1711.05394v1 (2nd paragraph, page 7) for
    # details about the value of a.
    a = 1 / (
        discretisation_point_number
        + -1  # Minus one to get the number of edges in the graph.
    )
    simulation_time = pde_evolution_time / a
    return simulation_time
