# ======================================================================
# Copyright CERFACS (February 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================


def compute_qubit_number_from_considered_points_1d(
    number_of_considered_points: int,
) -> int:
    r"""Compute the number of qubits needed to solve the wave equation in 1D.

    The size of the matrix :math:`H` considered is the sum of the number of columns and
    lines of :math:`B` because

    .. math::

        H \propto
        \begin{pmatrix}
        0 & B \\
        B^\dagger & 0 \\
        \end{pmatrix}.

    Let :math:`N` be the number of discretisation points.

    .. note::

        As the two boundary points **are not considered**,
        `number_of_considered_points` = :math:`N - 2`.

    :math:`B` has :math:`N-1` rows and :math:`N-2` columns by construction. This lead
    to a matrix :math:`H` of size :math:`\left( 2N - 3, 2N - 3\right)`.

    Consequently, the matrix :math:`H` acts on

    .. math::

        \begin{split}
        n =& \left\lceil\log_2\left( 2N-3 \right)\right\rceil \\
        =& \left\lceil\log_2\left( 2 \times \text{number_of_considered_points}+1
          \right)\right\rceil
        \end{split}

    qubits.

    :param number_of_considered_points: The number of points in the discretisation
        of the 1D line **MINUS** the number of points at the boundaries (2 points
        in the case of a 1D line).
    :return: the number of qubits needed to solve the wave equation for the given
        discretisation.
    """
    # return int(numpy.ceil(numpy.log2(2 * number_of_considered_points + 1)))
    return int(2 * number_of_considered_points).bit_length()
