FROM ubuntu:18.04

LABEL version="1.0" maintainer="SUAU Adrien <adrien.suau@cerfacs.fr"

ARG ROOT_DIR="/"
ARG USER_DIR="/user"
ARG QATHS_DIR="${USER_DIR}/qaths"

# Copy the whole project to the container
COPY ./ ${QATHS_DIR}

# See https://askubuntu.com/a/1013396
ENV DEBIAN_FRONTEND=noninteractive

# Make sure that Python and pip are installed
RUN apt-get update
RUN apt-get install -y python3 python3-pip
RUN python3 -m pip install -U pip

RUN python3 -V

# Install MyQLM
RUN python3 -m pip install myqlm

# Install qaths
RUN python3 -m pip install -e ${QATHS_DIR}
