# Contributing

## Issue reporting

If you find an issue **please** report it at 
[https://gitlab.com/Nelimee/qaths/issues](https://gitlab.com/Nelimee/qaths/issues).

## Code contribution

Every code contribution is welcomed. If you want to contribute with code, the steps
to follow are:

 1. Open an issue at 
    [https://gitlab.com/Nelimee/qaths/issues](https://gitlab.com/Nelimee/qaths/issues)
    explaining why you want to improve the code and how you plan to do it. 
 2. Code your improvement. Try to update the issue regularly with your progress
    in order to let others know what you are doing.
 3. When the code is ready, be sure to reformat **all** the source code with
    [black](https://github.com/ambv/black). 
 4. Wait for review/approval.
 
 ## Documentation improvement
 
 The documentation is part of the project and maintaining it is one of the most
 time-consuming task of the project. If you want to help with the documentation,
 correct typos, add sections or new algorithms you are more than welcomed.
 
 Documentation improvements follow the same steps as code contribution except 
 the reformatting part:
 
 1. Open an issue at 
    [https://gitlab.com/Nelimee/qaths/issues](https://gitlab.com/Nelimee/qaths/issues)
    explaining why you want to improve/change the documentation and what you 
    plan to do add/modify. 
 2. Write your improvement. Try to update the issue regularly with your progress
    in order to let others know what you are doing.
 3. Wait for review/approval.
 
