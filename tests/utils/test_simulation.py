# ======================================================================
# Copyright CERFACS (May 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import numpy

from qaths.utils.math import normalise, kron
from tests.utils.decorators import needsqat, usessimulator, randomised
from qaths.utils.simulation import simulate_routine
from qaths.utils.endian import BIG_ENDIAN
from tests.utils.matrices import X as X_matrix, I, H, ctrl

from qat.lang.AQASM import QRoutine, X as X_gate, CNOT, H as H_gate


@needsqat
@usessimulator
@randomised
def test_simulation_empty_routine():
    routine = QRoutine(arity=2)
    full_input_msb = normalise(numpy.random.rand(2 ** 2))
    expected_output_msb = full_input_msb.copy()

    output_state = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    numpy.testing.assert_allclose(expected_output_msb, output_state, atol=1e-7)


@needsqat
@usessimulator
@randomised
def test_simulation_X():
    routine = QRoutine(arity=2)
    routine.apply(X_gate, 0)
    full_input_msb = normalise(numpy.random.rand(2 ** 2))
    expected_output_msb = numpy.dot(kron(X_matrix, I), full_input_msb)

    print(full_input_msb)

    output_state = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    numpy.testing.assert_allclose(expected_output_msb, output_state, atol=1e-7)


@needsqat
@usessimulator
@randomised
def test_simulation_complex():
    routine = QRoutine(arity=3)
    routine.apply(H_gate, 0)
    routine.apply(H_gate, 1)
    routine.apply(H_gate, 2)
    routine.apply(CNOT, 1, 2)
    routine.apply(X_gate, 1)
    routine.apply(X_gate, 2)
    routine.apply(CNOT, 2, 1)

    full_input_msb = normalise(numpy.random.rand(2 ** 3))
    matrix = kron(H, H, H)
    matrix = numpy.dot(ctrl(X_matrix, 1, 2, 3), matrix)
    matrix = numpy.dot(kron(I, X_matrix, X_matrix), matrix)
    matrix = numpy.dot(ctrl(X_matrix, 2, 1, 3), matrix)
    expected_output_msb = numpy.dot(matrix, full_input_msb)

    output_state = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    numpy.testing.assert_allclose(expected_output_msb, output_state, atol=1e-7)
