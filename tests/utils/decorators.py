# ======================================================================
# Copyright CERFACS (March 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import pytest

from tests.utils.constants import RANDOMISED_REPEAT


def _qat_is_importable() -> bool:
    """Test if the qat library is importable or not."""
    try:
        import qat
    except ImportError:
        return False
    return True


"""Decorator to skip the decorated test if qat is not found."""
needsqat = pytest.mark.skipif(
    not _qat_is_importable(), reason="The qat library could not be found."
)

"""Decorator to mark tests that use qat's simulators."""
usessimulator = pytest.mark.usessimulator

"""Decorator to mark tests that may be slow."""
slow = pytest.mark.slow


def randomised(repeat_or_func):
    """Decorator to mark randomised tests."""
    if isinstance(repeat_or_func, int):
        repeat = repeat_or_func

        def _internal_decorator(func):
            return pytest.mark.randomised(pytest.mark.repeat(repeat)(func))

        return _internal_decorator
    else:
        func = repeat_or_func
        repeat = RANDOMISED_REPEAT
        return pytest.mark.randomised(pytest.mark.repeat(repeat)(func))
