# ======================================================================
# Copyright CERFACS (April 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import pytest
import numpy

from qaths.qram import (
    QRAM,
    PermutationOnlyQRAM,
    SignOnlyQRAM,
    UnsignedIntOnlyQRAM,
    UnsignedIntQRAM,
    PermutationSignQRAM,
)
from qaths.utils.quantum import qstate_msb

from tests.utils.decorators import needsqat, usessimulator, randomised
from qaths.utils.simulation import simulate_routine
from qaths.utils.math import kron, normalise, generate_random_hermitian_permutation
from qaths.utils.quantum import zero, one
from qaths.utils.endian import BIG_ENDIAN, LITTLE_ENDIAN


@needsqat
@usessimulator
@pytest.mark.parametrize(
    "n, given_input_msb, expected_output_msb",
    [
        (1, qstate_msb("0"), qstate_msb("0")),
        (1, qstate_msb("1"), qstate_msb("1")),
        (2, qstate_msb("00"), qstate_msb("00")),
        (2, qstate_msb("01"), qstate_msb("01")),
        (2, qstate_msb("10"), qstate_msb("10")),
        (2, qstate_msb("11"), qstate_msb("11")),
        (4, qstate_msb("1010"), qstate_msb("1010")),
        (4, qstate_msb("1111"), qstate_msb("1111")),
        (4, qstate_msb("0000"), qstate_msb("0000")),
    ],
)
def test_identity_qram(n: int, given_input_msb, expected_output_msb):
    identity = numpy.identity(2 ** n)
    routine = QRAM(identity, msb_first=True)
    full_input_msb = kron(given_input_msb, *[zero for _ in range(n)])
    full_expected_output_msb = kron(given_input_msb, expected_output_msb)
    output_msb = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )

    numpy.testing.assert_allclose(full_expected_output_msb, output_msb, atol=1e-7)


@needsqat
@usessimulator
@randomised
def test_msb_first_qram():
    m, n = 2, 2
    qram_data = numpy.array(
        [normalise(numpy.random.rand(2 ** m)) for _ in range(2 ** n)]
    )
    routine = QRAM(qram_data, msb_first=True)
    rand = numpy.random.randint(0, 2 ** n)
    initial_quantum_state = qstate_msb(bin(rand)[2:].zfill(n))
    input = kron(initial_quantum_state, *[zero for _ in range(m)])
    output = simulate_routine(
        routine,
        input,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    expected_output = kron(initial_quantum_state, qram_data[rand])
    numpy.testing.assert_allclose(expected_output, output, atol=1e-7)


@needsqat
@usessimulator
@randomised
def test_lsb_first_qram():
    m, n = 2, 2
    qram_data = numpy.array(
        [normalise(numpy.random.rand(2 ** m)) for _ in range(2 ** n)]
    )
    routine = QRAM(qram_data, msb_first=False)
    rand = numpy.random.randint(0, 2 ** n)
    initial_quantum_state_msb = qstate_msb(bin(rand)[2:].zfill(n))
    input = kron(*([zero for _ in range(m)] + [initial_quantum_state_msb]))
    output = simulate_routine(
        routine,
        input,
        starting_state_endianness=LITTLE_ENDIAN,
        output_state_endianness=LITTLE_ENDIAN,
    )
    expected_output = kron(qram_data[rand], initial_quantum_state_msb)
    numpy.testing.assert_allclose(expected_output, output, atol=1e-7)


@needsqat
@usessimulator
@pytest.mark.parametrize("n, m", [(1, 1), (1, 2), (2, 1), (2, 2)])
@randomised
def test_randomised_qram(n: int, m: int):
    qram_data = numpy.array(
        [normalise(numpy.random.rand(2 ** m)) for _ in range(2 ** n)]
    )
    routine = QRAM(qram_data, msb_first=True)

    rand = numpy.random.randint(0, 2 ** n)
    initial_quantum_state = qstate_msb(bin(rand)[2:].zfill(n))
    input = kron(initial_quantum_state, *[zero for _ in range(m)])
    output = simulate_routine(
        routine,
        input,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    expected_output = kron(initial_quantum_state, qram_data[rand])
    numpy.testing.assert_allclose(expected_output, output, atol=1e-7)


@needsqat
@usessimulator
@pytest.mark.parametrize(
    "n, given_input_msb, expected_output_msb",
    [
        (1, qstate_msb("0"), qstate_msb("0")),
        (1, qstate_msb("1"), qstate_msb("1")),
        (2, qstate_msb("00"), qstate_msb("00")),
        (2, qstate_msb("01"), qstate_msb("01")),
        (2, qstate_msb("10"), qstate_msb("10")),
        (2, qstate_msb("11"), qstate_msb("11")),
        (4, qstate_msb("1010"), qstate_msb("1010")),
        (4, qstate_msb("1111"), qstate_msb("1111")),
        (4, qstate_msb("0000"), qstate_msb("0000")),
    ],
)
def test_identity_permutation_qram(n: int, given_input_msb, expected_output_msb):
    identity = numpy.identity(2 ** n)
    routine = PermutationOnlyQRAM(identity)
    full_input_msb = kron(given_input_msb, *[zero for _ in range(n)])
    full_expected_output_msb = kron(given_input_msb, expected_output_msb)
    output_msb = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )

    numpy.testing.assert_allclose(full_expected_output_msb, output_msb, atol=1e-7)


@needsqat
@usessimulator
@pytest.mark.parametrize("n", list(range(1, 4)))
@randomised
def test_randomised_permutation_qram(n: int):
    identity = numpy.identity(2 ** n)
    hamiltonian_permutation = generate_random_hermitian_permutation(n)
    matrix = identity[hamiltonian_permutation]
    routine = PermutationOnlyQRAM(matrix)

    rand = numpy.random.randint(0, 2 ** n)
    initial_quantum_state = qstate_msb(bin(rand)[2:].zfill(n))
    full_input_msb = kron(initial_quantum_state, *[zero for _ in range(n)])
    output_msb = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    expected_output = kron(initial_quantum_state, matrix[rand])
    numpy.testing.assert_allclose(expected_output, output_msb, atol=1e-7)


@needsqat
@usessimulator
@pytest.mark.parametrize(
    "n, given_input_msb",
    [
        (1, qstate_msb("0")),
        (1, qstate_msb("1")),
        (2, qstate_msb("00")),
        (2, qstate_msb("01")),
        (2, qstate_msb("10")),
        (2, qstate_msb("11")),
        (4, qstate_msb("1010")),
        (4, qstate_msb("1111")),
        (4, qstate_msb("0000")),
    ],
)
def test_identity_unsigned_only_qram(n: int, given_input_msb):
    identity = numpy.identity(2 ** n)
    routine = UnsignedIntOnlyQRAM(identity, int_size=1)
    full_input_msb = kron(given_input_msb, zero)
    full_expected_output_msb = kron(given_input_msb, one)
    output_msb = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )

    numpy.testing.assert_allclose(full_expected_output_msb, output_msb, atol=1e-7)


@needsqat
@usessimulator
@pytest.mark.parametrize("n", list(range(1, 4)))
@pytest.mark.parametrize("int_size", list(range(1, 4)))
@randomised(3)
def test_randomised_unsigned_only_qram(n: int, int_size: int):
    # Create a diagonal matrix with random integer entries.
    identity_int = numpy.diag(numpy.random.randint(1, 2 ** int_size, size=2 ** n))
    # Compute a random permutation that will shuffle the identity matrix to a hermitian
    # matrix
    hamiltonian_permutation = generate_random_hermitian_permutation(n)
    # Shuffle
    matrix = identity_int[hamiltonian_permutation]
    # And create the QRAM routine that will be tested.
    routine = UnsignedIntOnlyQRAM(matrix, int_size=int_size)

    rand = numpy.random.randint(0, 2 ** n)
    initial_quantum_state = qstate_msb(bin(rand)[2:].zfill(n))
    full_input_msb = kron(initial_quantum_state, *[zero for _ in range(int_size)])
    output_msb = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    only_non_zero = matrix[rand][matrix[rand].nonzero()][0]
    expected_output = kron(
        initial_quantum_state, qstate_msb(bin(only_non_zero)[2::].zfill(int_size))
    )
    numpy.testing.assert_allclose(expected_output, output_msb, atol=1e-7)


@needsqat
@usessimulator
@pytest.mark.parametrize(
    "n, given_input_msb, expected_output_msb",
    [
        (1, qstate_msb("0"), qstate_msb("0")),
        (1, qstate_msb("1"), qstate_msb("1")),
        (2, qstate_msb("00"), qstate_msb("00")),
        (2, qstate_msb("01"), qstate_msb("01")),
        (2, qstate_msb("10"), qstate_msb("10")),
        (2, qstate_msb("11"), qstate_msb("11")),
        (4, qstate_msb("1010"), qstate_msb("1010")),
        (4, qstate_msb("1111"), qstate_msb("1111")),
        (4, qstate_msb("0000"), qstate_msb("0000")),
    ],
)
@pytest.mark.parametrize("int_size", list(range(1, 4)))
def test_identity_unsigned_qram(
    n: int,
    given_input_msb: numpy.ndarray,
    expected_output_msb: numpy.ndarray,
    int_size: int,
):
    identity = numpy.identity(2 ** n)
    routine = UnsignedIntQRAM(identity, int_size=int_size)
    full_input_msb = kron(given_input_msb, *[zero for _ in range(n + int_size)])
    full_expected_output_msb = kron(
        given_input_msb,
        expected_output_msb,
        *([zero for _ in range(int_size - 1)] + [one])
    )
    output_msb = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )

    numpy.testing.assert_allclose(full_expected_output_msb, output_msb, atol=1e-7)


@needsqat
@usessimulator
@pytest.mark.parametrize("n", list(range(1, 4)))
@pytest.mark.parametrize("int_size", list(range(1, 4)))
@randomised(3)
def test_randomised_unsigned_qram(n: int, int_size: int):
    # Create a diagonal matrix with random integer entries.
    identity_int = numpy.diag(numpy.random.randint(1, 2 ** int_size, size=2 ** n))
    # Compute a random permutation that will shuffle the identity matrix to a hermitian
    # matrix
    hamiltonian_permutation = generate_random_hermitian_permutation(n)
    # Shuffle
    matrix = identity_int[hamiltonian_permutation]
    # And create the QRAM routine that will be tested.
    routine = UnsignedIntQRAM(matrix, int_size=int_size)

    rand = numpy.random.randint(0, 2 ** n)
    initial_quantum_state = qstate_msb(bin(rand)[2:].zfill(n))
    full_input_msb = kron(initial_quantum_state, *[zero for _ in range(n + int_size)])
    output_msb = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    only_non_zero = matrix[rand][matrix[rand].nonzero()][0]
    expected_output = kron(
        initial_quantum_state,
        (numpy.abs(matrix[rand]) > 0.5),
        qstate_msb(bin(only_non_zero)[2::].zfill(int_size)),
    )
    numpy.testing.assert_allclose(expected_output, output_msb, atol=1e-7)


@needsqat
@usessimulator
@pytest.mark.parametrize(
    "n, given_input_msb",
    [
        (1, qstate_msb("0")),
        (1, qstate_msb("1")),
        (2, qstate_msb("00")),
        (2, qstate_msb("01")),
        (2, qstate_msb("10")),
        (2, qstate_msb("11")),
        (4, qstate_msb("1010")),
        (4, qstate_msb("1111")),
        (4, qstate_msb("0000")),
    ],
)
def test_identity_sign_only_qram(n: int, given_input_msb):
    identity = numpy.identity(2 ** n)
    routine = SignOnlyQRAM(identity)
    full_input_msb = kron(given_input_msb, zero)
    full_expected_output_msb = kron(given_input_msb, zero)
    output_msb = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )

    numpy.testing.assert_allclose(full_expected_output_msb, output_msb, atol=1e-7)


@needsqat
@usessimulator
@pytest.mark.parametrize("n", list(range(1, 4)))
@randomised
def test_randomised_sign_only_qram(n: int):
    # Create a diagonal matrix with random +/-1 entries.
    identity_int = numpy.diag(numpy.random.choice([-1, 1], replace=True, size=2 ** n))
    # Compute a random permutation that will shuffle the identity matrix to a hermitian
    # matrix
    hamiltonian_permutation = generate_random_hermitian_permutation(n)
    # Shuffle
    matrix = identity_int[hamiltonian_permutation]
    # And create the QRAM routine that will be tested.
    routine = SignOnlyQRAM(matrix)

    rand = numpy.random.randint(0, 2 ** n)
    initial_quantum_state = qstate_msb(bin(rand)[2:].zfill(n))
    full_input_msb = kron(initial_quantum_state, zero)
    output_msb = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    only_non_zero = matrix[rand][matrix[rand].nonzero()][0]
    expected_output = kron(initial_quantum_state, one if only_non_zero < 0 else zero)
    numpy.testing.assert_allclose(expected_output, output_msb, atol=1e-7)


@needsqat
@usessimulator
@pytest.mark.parametrize(
    "n, given_input_msb, expected_output_msb",
    [
        (1, qstate_msb("0"), qstate_msb("0")),
        (1, qstate_msb("1"), qstate_msb("1")),
        (2, qstate_msb("00"), qstate_msb("00")),
        (2, qstate_msb("01"), qstate_msb("01")),
        (2, qstate_msb("10"), qstate_msb("10")),
        (2, qstate_msb("11"), qstate_msb("11")),
        (4, qstate_msb("1010"), qstate_msb("1010")),
        (4, qstate_msb("1111"), qstate_msb("1111")),
        (4, qstate_msb("0000"), qstate_msb("0000")),
    ],
)
def test_identity_permutation_sign_qram(n: int, given_input_msb, expected_output_msb):
    identity = numpy.identity(2 ** n)
    routine = PermutationSignQRAM(identity)
    full_input_msb = kron(given_input_msb, *[zero for _ in range(n + 1)])
    full_expected_output_msb = kron(given_input_msb, expected_output_msb, zero)
    output_msb = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )

    numpy.testing.assert_allclose(full_expected_output_msb, output_msb, atol=1e-7)


@needsqat
@usessimulator
@pytest.mark.parametrize("n", list(range(1, 4)))
@randomised
def test_randomised_unsigned_qram(n: int):
    # Create a diagonal matrix with random +/-1 entries.
    identity_int = numpy.diag(numpy.random.choice([-1, 1], replace=True, size=2 ** n))
    # Compute a random permutation that will shuffle the identity matrix to a hermitian
    # matrix
    hamiltonian_permutation = generate_random_hermitian_permutation(n)
    # Shuffle
    matrix = identity_int[hamiltonian_permutation]
    # And create the QRAM routine that will be tested.
    routine = PermutationSignQRAM(matrix)

    rand = numpy.random.randint(0, 2 ** n)
    initial_quantum_state = qstate_msb(bin(rand)[2:].zfill(n))
    full_input_msb = kron(initial_quantum_state, *[zero for _ in range(n + 1)])
    output_msb = simulate_routine(
        routine,
        full_input_msb,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    only_non_zero = matrix[rand][matrix[rand].nonzero()][0]
    expected_output = kron(
        initial_quantum_state,
        qstate_msb(bin(hamiltonian_permutation[rand])[2::].zfill(n)),
        one if only_non_zero < 0 else zero,
    )
    numpy.testing.assert_allclose(expected_output, output_msb, atol=1e-7)
