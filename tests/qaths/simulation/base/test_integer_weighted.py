# ======================================================================
# Copyright CERFACS (May 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import numpy
import pytest
import qaths.simulation.base.integer_weighted as integer_routines
from qaths.qram import UnsignedIntQRAM, SignedIntQRAM
from qaths.utils import constants
from qaths.utils.math import expm, kron, generate_random_hermitian_permutation
from qaths.utils.printing import transformation
from qaths.utils.simulation import routine2unitary

import tests.utils.constants as test_constants
from tests.utils import matrices
from tests.utils.decorators import needsqat, randomised


def _test_matrix_on_simulate_unsigned_integer_weighted_hamiltonian(
    oracle, matrix_to_simulate: numpy.ndarray, time: float, n: int, int_size: int
):
    routine = integer_routines.simulate_unsigned_integer_weighted_hamiltonian(
        oracle, n, int_size, time
    )

    expected_routine_arity = 2 * n + int_size + 1
    routine_ancilla_size = routine.arity - expected_routine_arity

    expected_matrix = kron(
        expm(-1.0j * time * matrix_to_simulate),
        numpy.identity(2 ** (expected_routine_arity - n)),
        numpy.identity(2 ** routine_ancilla_size) if routine_ancilla_size > 0 else 1,
    )
    simulated_matrix = routine2unitary(routine)

    # We restrict ourselves to the cases where |m> = |v> = |p> = |a> = |0>
    # because this is a precondition of the simulate_real_permutation_hamiltonian function.
    # This means that every row of the expected or simulated matrix that correspond
    # to a quantum state with either |m>, |v>, |p>, or |a> not equal to |0> must be
    # ignored.
    rows_to_test = numpy.arange(0, 2 ** routine.arity, 2 ** (routine.arity - n))

    numpy.testing.assert_allclose(
        expected_matrix[rows_to_test],
        simulated_matrix[rows_to_test],
        atol=constants.ABSOLUTE_TOLERANCE,
    )


def _test_matrix_on_simulate_signed_integer_weighted_hamiltonian(
    oracle, matrix_to_simulate: numpy.ndarray, time: float, n: int, int_size: int
):
    routine = integer_routines.simulate_signed_integer_weighted_hamiltonian(
        oracle, n, int_size, time
    )

    expected_routine_arity = 2 * n + int_size + 2
    routine_ancilla_size = routine.arity - expected_routine_arity

    expected_matrix = kron(
        expm(-1.0j * time * matrix_to_simulate),
        numpy.identity(2 ** (expected_routine_arity - n)),
        numpy.identity(2 ** routine_ancilla_size) if routine_ancilla_size > 0 else 1,
    )
    simulated_matrix = routine2unitary(routine)

    # We restrict ourselves to the cases where |m> = |v> = |p> = |a> = |0>
    # because this is a precondition of the simulate_real_permutation_hamiltonian function.
    # This means that every row of the expected or simulated matrix that correspond
    # to a quantum state with either |m>, |v>, |p>, or |a> not equal to |0> must be
    # ignored.
    rows_to_test = numpy.arange(0, 2 ** routine.arity, 2 ** (routine.arity - n))

    numpy.testing.assert_allclose(
        expected_matrix[rows_to_test],
        simulated_matrix[rows_to_test],
        atol=constants.ABSOLUTE_TOLERANCE,
    )


def _test_matrix_on_simulate_imaginary_integer_weighted_hamiltonian(
    oracle, matrix_to_simulate: numpy.ndarray, time: float, n: int, int_size: int
):
    routine = integer_routines.simulate_imaginary_integer_weighted_hamiltonian(
        oracle, n, int_size, time
    )

    expected_routine_arity = 2 * n + int_size + 2
    routine_ancilla_size = routine.arity - expected_routine_arity

    expected_matrix = kron(
        expm(-1.0j * time * matrix_to_simulate),
        numpy.identity(2 ** (expected_routine_arity - n)),
        numpy.identity(2 ** routine_ancilla_size) if routine_ancilla_size > 0 else 1,
    )
    simulated_matrix = routine2unitary(routine)

    # We restrict ourselves to the cases where |m> = |v> = |p> = |a> = |0>
    # because this is a precondition of the simulate_real_permutation_hamiltonian function.
    # This means that every row of the expected or simulated matrix that correspond
    # to a quantum state with either |m>, |v>, |p>, or |a> not equal to |0> must be
    # ignored.
    rows_to_test = numpy.arange(0, 2 ** routine.arity, 2 ** (routine.arity - n))

    print(
        "Expected:\n{}".format(
            transformation(expected_matrix, rows_to_print=rows_to_test)
        )
    )
    print("=" * 80)
    print(
        "Simulated:\n{}".format(
            transformation(simulated_matrix, rows_to_print=rows_to_test)
        )
    )

    numpy.testing.assert_allclose(
        expected_matrix[rows_to_test],
        simulated_matrix[rows_to_test],
        atol=constants.ABSOLUTE_TOLERANCE,
    )


@needsqat
@pytest.mark.parametrize(
    "n, matrix_to_simulate",
    # Simulation of the identity matrix for different sizes
    [(i, numpy.identity(2 ** i)) for i in range(1, 3)]
    # Simulation of the X matrix
    + [(1, matrices.X)]
    # Simulation of the CX matrix
    + [(2, matrices.CX)]
    # User defined matrices
    + [
        # Not really special
        (2, numpy.array([[0, 1, 0, 0], [1, 0, 0, 0], [0, 0, 0, 1], [0, 0, 1, 0]])),
        # Some rows with no non-zero elements
        (2, numpy.array([[0, 0, 1, 0], [0, 0, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0]])),
        # Only one 1 and then zeros
        (2, numpy.array([[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])),
        # Only zeros
        (2, numpy.array([[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])),
    ],
)
@pytest.mark.parametrize("time", test_constants.FLOAT_DISCRETISATION)
@pytest.mark.parametrize("int_size", list(range(1, 3)))
def test_simulate_unsigned_with_unsigned_hamiltonian(
    n: int, matrix_to_simulate: numpy.ndarray, time: float, int_size: int
):
    oracle = UnsignedIntQRAM(matrix_to_simulate, int_size=int_size)
    _test_matrix_on_simulate_unsigned_integer_weighted_hamiltonian(
        oracle, matrix_to_simulate, time, n, int_size
    )


@needsqat
@pytest.mark.parametrize("n", list(range(1, 3)))
@pytest.mark.parametrize("time", test_constants.FLOAT_DISCRETISATION)
@pytest.mark.parametrize("int_size", list(range(1, 4)))
@randomised(3)
def test_simulate_unsigned_with_unsigned_hamiltonian_randomised(
    n: int, time: float, int_size: int
):
    # Generate the permutation that will be used here.
    hermitian_permutation = generate_random_hermitian_permutation(n)
    # Generate the base values.
    diag_values = numpy.random.randint(0, 2 ** int_size, 2 ** n)
    # Make the generated values symmetrical with respect to the permutation,
    # i.e. diag_values == diag_values[hermitian_permutation]
    diag_values = (diag_values + diag_values[hermitian_permutation]) // 2
    # Create the matrix and permute.
    matrix_to_simulate = numpy.diag(diag_values)[hermitian_permutation]

    oracle = UnsignedIntQRAM(matrix_to_simulate, int_size=int_size)
    _test_matrix_on_simulate_unsigned_integer_weighted_hamiltonian(
        oracle, matrix_to_simulate, time, n, int_size
    )


@needsqat
@pytest.mark.parametrize(
    "n, matrix_to_simulate",
    # Simulation of the identity matrix for different sizes
    [(i, numpy.identity(2 ** i)) for i in range(1, 3)]
    # Simulation of the X matrix
    + [(1, matrices.X)]
    # Simulation of the CX matrix
    + [(2, matrices.CX)]
    # Negatives counterparts
    + [(i, -numpy.identity(2 ** i)) for i in range(1, 3)]
    + [(1, -matrices.X)]
    + [(2, -matrices.CX)]
    # User defined matrices
    + [
        # Not really special
        (2, numpy.array([[0, 1, 0, 0], [1, 0, 0, 0], [0, 0, 0, 1], [0, 0, 1, 0]])),
        # Some rows with no non-zero elements
        (2, numpy.array([[0, 0, 1, 0], [0, 0, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0]])),
        # Only one 1 and then zeros
        (2, numpy.array([[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])),
        # Only zeros
        (2, numpy.array([[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])),
        # Some negative entries
        (2, numpy.array([[0, -1, 0, 0], [-1, 0, 0, 0], [0, 0, 0, 1], [0, 0, 1, 0]])),
        # Some rows with no non-zero elements and negative entries
        (2, numpy.array([[0, 0, -1, 0], [0, 0, 0, 0], [-1, 0, 0, 0], [0, 0, 0, 0]])),
        # Only one -1 and then zeros
        (2, numpy.array([[-1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])),
    ],
)
@pytest.mark.parametrize("time", test_constants.FLOAT_DISCRETISATION)
@pytest.mark.parametrize("int_size", list(range(1, 3)))
def test_simulate_unsigned_with_signed_hamiltonian(
    n: int, matrix_to_simulate: numpy.ndarray, time: float, int_size: int
):
    oracle = SignedIntQRAM(matrix_to_simulate, int_size=int_size)
    _test_matrix_on_simulate_signed_integer_weighted_hamiltonian(
        oracle, matrix_to_simulate, time, n, int_size
    )


@needsqat
@pytest.mark.parametrize("n", list(range(1, 3)))
@pytest.mark.parametrize("time", test_constants.FLOAT_DISCRETISATION)
@pytest.mark.parametrize("int_size", list(range(1, 4)))
@randomised(3)
def test_simulate_unsigned_with_signed_hamiltonian_randomised(
    n: int, time: float, int_size: int
):
    # Generate the permutation that will be used here.
    hermitian_permutation = generate_random_hermitian_permutation(n)
    # Generate the base values.
    diag_values = numpy.random.randint(-(2 ** int_size) + 1, 2 ** int_size, 2 ** n)
    # Make the generated values symmetrical with respect to the permutation,
    # i.e. diag_values == diag_values[hermitian_permutation]
    diag_values = (diag_values + diag_values[hermitian_permutation]) // 2
    # Create the matrix and permute.
    matrix_to_simulate = numpy.diag(diag_values)[hermitian_permutation]

    oracle = SignedIntQRAM(matrix_to_simulate, int_size=int_size)
    _test_matrix_on_simulate_signed_integer_weighted_hamiltonian(
        oracle, matrix_to_simulate, time, n, int_size
    )


@needsqat
@pytest.mark.parametrize(
    "n, matrix_to_simulate",
    # Simulation of the Y matrix
    [(1, matrices.Y)]
    # Negatives counterparts
    + [(1, -matrices.Y)]
    # User defined matrices
    + [
        # Not really special
        (
            2,
            numpy.array(
                [[0, 1.0j, 0, 0], [-1.0j, 0, 0, 0], [0, 0, 0, 1.0j], [0, 0, -1.0j, 0]]
            ),
        ),
        # Some rows with no non-zero elements
        (
            2,
            numpy.array(
                [[0, 0, -1.0j, 0], [0, 0, 0, 0], [1.0j, 0, 0, 0], [0, 0, 0, 0]]
            ),
        ),
        # Only zeros
        (2, numpy.array([[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])),
    ],
)
@pytest.mark.parametrize("time", test_constants.FLOAT_DISCRETISATION)
@pytest.mark.parametrize("int_size", list(range(1, 3)))
def test_simulate_unsigned_with_signed_hamiltonian(
    n: int, matrix_to_simulate: numpy.ndarray, time: float, int_size: int
):
    oracle = SignedIntQRAM(matrix_to_simulate.imag, int_size=int_size)
    _test_matrix_on_simulate_imaginary_integer_weighted_hamiltonian(
        oracle, matrix_to_simulate, time, n, int_size
    )


@needsqat
@pytest.mark.parametrize("n", list(range(1, 3)))
@pytest.mark.parametrize("time", test_constants.FLOAT_DISCRETISATION)
@pytest.mark.parametrize("int_size", list(range(1, 4)))
@randomised(3)
def test_simulate_unsigned_with_signed_hamiltonian_randomised(
    n: int, time: float, int_size: int
):
    # Generate the permutation that will be used here.
    hermitian_permutation, signs = generate_random_hermitian_permutation(
        n, include_conj=True
    )
    # Generate the base values.
    diag_values = numpy.random.randint(
        -(2 ** int_size) + 1, 2 ** int_size, 2 ** n
    ).astype(numpy.complex)
    # Make the generated values symmetrical with respect to the permutation,
    # i.e. diag_values == diag_values[hermitian_permutation]
    diag_values = (diag_values + diag_values[hermitian_permutation]) // 2
    for i in range(2 ** n):
        if signs[i]:
            diag_values[i] = -1.0j * diag_values[i]
        else:
            diag_values[i] = 1.0j * diag_values[i]
    # Create the matrix and permute.
    matrix_to_simulate = numpy.diag(diag_values)[hermitian_permutation]

    oracle = SignedIntQRAM(matrix_to_simulate.imag, int_size=int_size)
    _test_matrix_on_simulate_imaginary_integer_weighted_hamiltonian(
        oracle, matrix_to_simulate, time, n, int_size
    )
