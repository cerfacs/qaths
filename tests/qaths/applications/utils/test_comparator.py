# ======================================================================
# Copyright CERFACS (May 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import numpy
import pytest
from qaths.routines.comparator import compare, equals, arithmetic_compare
from qaths.utils import constants
from qaths.utils.math import kron, normalise
from qaths.utils.quantum import zero, one
from qaths.utils.simulation import simulate_routine

from tests.utils.decorators import needsqat, usessimulator


@needsqat
@usessimulator
@pytest.mark.parametrize(
    "n, given_input, compare_to, expected_output",
    [(1, 0, 0, False), (1, 0, 1, True), (1, 1, 1, False)]
    + [(2, i, j, i < j) for i in range(2 ** 2) for j in range(2 ** 2)]
    + [(3, i, j, i < j) for i in range(2 ** 3) for j in range(2 ** 3)],
)
def test_compare(n: int, given_input: int, compare_to: int, expected_output: bool):
    initial_state = numpy.zeros((2 ** n))
    initial_state[given_input] = 1.0

    routine = compare(n, compare_to)
    output = simulate_routine(routine, starting_state=kron(initial_state, zero, zero))
    full_expected_output = kron(initial_state, zero, one if expected_output else zero)

    numpy.testing.assert_allclose(
        output, full_expected_output, atol=constants.ABSOLUTE_TOLERANCE
    )


@needsqat
@usessimulator
@pytest.mark.parametrize(
    "n, given_input, compare_to, expected_output",
    [(1, 0, 0, True), (1, 0, 1, False), (1, 1, 0, False), (1, 1, 1, True)]
    + [(2, i, j, i == j) for i in range(2 ** 2) for j in range(2 ** 2)]
    + [(3, i, j, i == j) for i in range(2 ** 3) for j in range(2 ** 3)],
)
def test_equals(n: int, given_input: int, compare_to: int, expected_output: bool):
    initial_state = numpy.zeros((2 ** n))
    initial_state[given_input] = 1.0

    routine = equals(n, compare_to)
    output = simulate_routine(
        routine, starting_state=kron(initial_state, zero, zero, zero, zero)
    )
    full_expected_output = kron(
        initial_state, one if expected_output else zero, zero, zero, zero
    )

    numpy.testing.assert_allclose(
        output, full_expected_output, atol=constants.ABSOLUTE_TOLERANCE
    )


@needsqat
@usessimulator
@pytest.mark.parametrize(
    "n, given_input, compare_to, expected_output",
    [(1, 0, 0, False), (1, 0, 1, True), (1, 1, 0, False), (1, 1, 1, False)]
    + [(2, i, j, i < j) for i in range(2 ** 2) for j in range(2 ** 2)]
    + [(3, i, j, i < j) for i in range(2 ** 3) for j in range(2 ** 3)],
)
def test_arithmetic_compare(
    n: int, given_input: int, compare_to: int, expected_output: bool
):
    initial_state = numpy.zeros((2 ** n))
    initial_state[given_input] = 1.0
    dirty_state = normalise(numpy.random.rand(2 ** (n - 1)))

    routine = arithmetic_compare(n, compare_to)
    output = simulate_routine(
        routine, starting_state=kron(initial_state, zero, dirty_state)
    )
    full_expected_output = kron(
        initial_state, one if expected_output else zero, dirty_state
    )

    numpy.testing.assert_allclose(
        output, full_expected_output, atol=constants.ABSOLUTE_TOLERANCE
    )
