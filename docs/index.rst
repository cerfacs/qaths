qaths: A Python 3 library of Hamiltonian simulation implementations
===================================================================
The qaths library aims at implementing various Hamiltonian Simulation algorithms
with the help of `qat`, the Atos Python library for quantum computing.

User Documentation
------------------

.. toctree::
    :maxdepth: 2

    pages/install
    pages/algorithms
    pages/endianness


API Reference
-------------

.. toctree::
    :maxdepth: 2

    pages/api-ref

Bibliography
------------

.. toctree::
    :maxdepth: 1

    pages/zbibliography


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
