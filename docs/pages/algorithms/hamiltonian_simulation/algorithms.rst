..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.

Algorithms
==========

List of Hamiltonian simulation algorithms
-----------------------------------------

.. toctree::
    :maxdepth: 2
    :numbered:

    algorithms/product_formula
    algorithms/quantum_walk
    algorithms/series
    algorithms/other

Complexity comparison
---------------------

+------------------------+----------------------------+------------------------+
|                        |                            |                        |
|       Algorithm        |      Query complexity      |Additional gates needed |
|                        |                            |                        |
+------------------------+----------------------------+------------------------+
|                        |                            |                        |
|    Product-formulas    |      |querycompl_PF|       |     |addcompl_PF|      |
|                        |                            |                        |
+------------------------+----------------------------+------------------------+
|  | Randomised          |                            |                        |
|  | product-formulas    |      |querycompl_rPF|      |     |addcompl_rPF|     |
|    [#notcomplete]_     |                            |                        |
+------------------------+----------------------------+------------------------+
|  | Divide and conquer  |                            |                        |
|  | product formula     |     |querycompl_dcpf|      |    |addcompl_dcpf|     |
|    [#notdone]_         |                            |                        |
+------------------------+----------------------------+------------------------+
|                        |                            |                        |
|  Taylor series (LCU    |      |querycompl_TS|       |     |addcompl_TS|      |
|  [#LCU]_)              |                            |                        |
+------------------------+----------------------------+------------------------+
|                        |                            |                        |
|     Quantum walks      |     |querycompl_qwalk|     |    |addcompl_qwalk|    |
|                        |                            |                        |
+------------------------+----------------------------+------------------------+
|                        |                            |                        |
|      Qubitization      | |querycompl_qubitization|  ||addcompl_qubitization| |
|                        |                            |                        |
+------------------------+----------------------------+------------------------+
|                        |                            |                        |
| | Lin. comb. of        |                            |                        |
| | quantum walk steps   |   |querycompl_linqwalks|   |  |addcompl_linqwalks|  |
|                        |                            |                        |
|                        |                            |                        |
+------------------------+----------------------------+------------------------+
|                        |                            |                        |
| | Quantum Signal       |      |querycompl_qsp|      |     |addcompl_qsp|     |
| | Processing           |                            |                        |
|                        |                            |                        |
+------------------------+----------------------------+------------------------+
|                        |                            |                        |
| | Uniform Spectral     |  |querycompl_uspectampl|   | |addcompl_uspectampl|  |
| | Amplification        |                            |                        |
|                        |                            |                        |
+------------------------+----------------------------+------------------------+


.. |querycompl_PF| replace::
   :math:`\mathcal{O} \left(5^{2k} m \left( m \tau \right)^{1+1/2k} / \epsilon^{1/2k}\right)`

.. |addcompl_PF| replace::
   :math:`\mathcal{O} \left(5^{2k} \left( n + r_{\text{prec}} \right) m \left( m \tau \right)^{1+1/2k} / \epsilon^{1/2k}\right)`

.. |querycompl_rPF| replace::
   :math:`\mathcal{O} \left(5^k m^2 t \max \left\{\left( \frac{mt}{\epsilon} \right)^\frac{1}{4k+1}, \left( \frac{t}{\epsilon} \right)^\frac{1}{2k}\right\}\right)`

.. |addcompl_rPF| replace::
   :math:`\mathcal{O} \left(5^k m^2 t \left( n + r_{\text{prec}} \right) \max \left\{\left( \frac{mt}{\epsilon} \right)^\frac{1}{4k+1}, \left( \frac{t}{\epsilon} \right)^\frac{1}{2k}\right\}\right)`

.. |querycompl_dcpf| replace::
   ❌

.. |addcompl_dcpf| replace::
   ❌

.. |querycompl_TS| replace::
   :math:`\mathcal{O}\left(\tau \frac{\log \frac{\tau}{\epsilon}}{\log \log \frac{\tau}{\epsilon}}\right)`

.. |addcompl_TS| replace::
   :math:`\mathcal{O}\left(n \tau \frac{\log^2 \frac{\tau}{\epsilon}}{\log \log \frac{\tau}{\epsilon}}\right)`

.. |querycompl_qwalk| replace::
   :math:`\mathcal{O}\left( \frac{\Lambda_H t}{\sqrt{\epsilon}} + s \Lambda^{max}_Ht \right)`

.. |addcompl_qwalk| replace::
   :math:`\mathcal{O} \left( s^{2/3} \left[\left(\log \log s \right) \Lambda_H t\right]^{4/3}\epsilon^{-1/3}\right)`

.. |querycompl_qubitization| replace::
   :math:`\Theta \left( t + \frac{\log \frac{1}{\epsilon}}{\log \log \frac{1}{\epsilon}} \right)`

.. |addcompl_qubitization| replace::
   :math:`\mathcal{O} \left( t + \frac{\log \frac{1}{\epsilon}}{\log \log \frac{1}{\epsilon}} \right)`

.. |querycompl_linqwalks| replace::
   :math:`\mathcal{O}\left( \tau \frac{\log \frac{\tau}{\epsilon}}{\log \log \frac{\tau}{\epsilon}} \right)`

.. |addcompl_linqwalks| replace::
   :math:`\mathcal{O}\left( \tau \left[ n + \log^{5/2}\left( \frac{\tau}{\epsilon} \right) \right] \frac{\log \frac{\tau}{\epsilon}}{\log \log \frac{\tau}{\epsilon}} \right)`

.. |querycompl_qsp| replace::
   :math:`\mathcal{O}\left( ts \Lambda^{max}_H + \frac{\log \frac{1}{\epsilon}}{\log\log \frac{1}{\epsilon}}\right)`

.. |addcompl_qsp| replace::
   :math:`\mathcal{O} \left( n + r \ \text{poly}\left( \log r \right) \right)`

.. |querycompl_uspectampl| replace::
   :math:`\mathcal{O} \left( t \sqrt{s \Lambda^{max}_H \Lambda^{1}_H}\log \left( \frac{t\Lambda_H}{\epsilon} \right) \left( 1 +\frac{1}{t\Lambda^{1}_H} \frac{\log \frac{1}{\epsilon}}{\log \log \frac{1}{\epsilon}}\right)\right)`

.. |addcompl_uspectampl| replace::
   ❌


.. [#notcomplete] The complexity analysis in the corresponding article shows weaknesses
                  and may be false or lack of details. Refers to the corresponding
                  article to check the validity of the complexity in your specific
                  case.

.. [#notdone] The complexity analysis of this algorithm has not been verified entirely.

.. [#LCU] Linear Combination of Unitaries
