..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.

Problem presentation and notations
==================================

.. _hamiltonian-problem-presentation:

Problem presentation
--------------------

The Hamiltonian simulation problem can be summarised as follow:

Given a time :math:`t > 0`, an Hermitian matrix :math:`H` and an allowed error
:math:`\epsilon > 0`, find a sequence of quantum gates that approximates the
unitary matrix :math:`e^{-iHt}` with a spectral error of at most :math:`\epsilon`.

In other words, find a sequence of quantum gates :math:`U` such that

.. math::

   \left\vert\left\vert U - e^{iHt} \right\vert\right\vert \leqslant \epsilon

where :math:`||\cdot||` is the spectral-norm.


Notations
---------

Complexity
~~~~~~~~~~

The `big-O notation <https://en.wikipedia.org/wiki/Big_O_notation#Family_of_Bachmann%E2%80%93Landau_notations>`__
is used in all the section.

.. admonition:: Note about logarithms

   The functions :math:`\ln` and :math:`\log` are used interchangeably in the
   complexities using big-O notation. The reason behind this is that the big-O
   notation only cares about asymptotic behaviour and so the constant factors
   can be removed from the expression. The functions :math:`\ln` and :math:`\log`
   being the same up to a constant factor of :math:`\ln(10)`, they have the
   same asymptotic behaviour.

Norms
~~~~~

Several norms are used within this section:

#. The induced :math:`1`-norm:

   .. math::

      ||\cdot||_1 = \max_j \sum_{k=1}^N \vert H_{j,k} \vert

#. The spectral-norm :math:`||\cdot||` defined as the maximum singular value of the
   matrix.
#. The :math:`\infty`-norm:

   .. math::

      ||\cdot||_{\infty} = \max_{j,k} \vert H_{j,k}\vert

Along with these norms, we denote by :math:`\Lambda_A` (resp. :math:`\Lambda_A^1`
and :math:`\Lambda_A^{\infty}`) an upper-bound of :math:`||A||` (resp.
:math:`||A||_1` and :math:`||A||_{\infty}`) for a given matrix :math:`A`.

Matrices and oracles
~~~~~~~~~~~~~~~~~~~~

If not precised otherwise:

* Capital letters like :math:`A`, :math:`B`, :math:`U` or :math:`H` represent a matrix.

  * :math:`U` represents a unitary matrix.
  * :math:`H` represents a Hamiltonian matrix.
  * **Warning**: :math:`O` represents an **oracle**.

* Capital letters with an hat like :math:`\hat{A}`, :math:`\hat{B}`, :math:`\hat{U}` or
  :math:`\hat{H}` represent an oracle. An alternative representation may be encountered
  with a capital :math:`O` and a possible subscript: :math:`O_H` represents an oracle
  for the matrix :math:`H`.

  .. warning::
     The oracle :math:`O` should not be confused with the big-O notation
     :math:`\mathcal{O}`.

Variables
~~~~~~~~~

+--------------------+------------------------------------+
|                    |                                    |
| Symbol             | Signification                      |
|                    |                                    |
+--------------------+------------------------------------+
|                    |                                    |
| :math:`s`          | Sparsity of the considered matrix  |
|                    |                                    |
+--------------------+------------------------------------+
|                    |                                    |
| :math:`\epsilon`   | Allowed error for the simulation   |
|                    |                                    |
+--------------------+------------------------------------+
|                    | The number of qubits :math:`H`     |
| :math:`n`          | acts on. :math:`H` is a            |
|                    | :math:`2^n \times 2^n` matrix.     |
+--------------------+------------------------------------+
|                    | | The number of qubits used to     |
| :math:`m`          |   represent a number (i.e. the     |
|                    |   precision).                      |
|                    | | Can be for both integer and real |
|                    |   numbers.                         |
+--------------------+------------------------------------+
|                    |                                    |
| :math:`t`          | Time of the desired simulation.    |
|                    |                                    |
+--------------------+------------------------------------+
|                    | :math:`||H||t` or                  |
| :math:`\tau`       | :math:`s^2||H||t` depending on the |
|                    | algorithm.                         |
+--------------------+------------------------------------+





