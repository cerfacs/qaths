..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.

Algorithms based on product formulas (Trotter formula)
======================================================


.. toctree::
    :maxdepth: 1

    product_formula/direct_trotter
    product_formula/divide_and_conquer
    product_formula/randomised_direct_trotter


❌Generic structure of the product-formula algorithms
-----------------------------------------------------

Decomposing the Hamiltonian
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first step of the algorithm is to decompose the input Hamiltonian matrix
:math:`H` into a sum Hamiltonians that are efficiently simulable.

If the input Hamiltonian is already efficiently simulable, this step is trivial
as it comes down to write

.. math::

    H = H_0 = \sum_{j=0}^0 H_j.

If :math:`H` is not already efficiently simulable then the decomposition
:math:`H = \sum_{j=0}^m H_j` will depend on the structure of :math:`H`.

For :ref:`sparse-hamiltonians`: according to
:cite:`2004-ahokas-graeme-robert-improved-algorithms-for-approximate-quantum-fourier-transforms-and-sparse-hamiltonian-simulations`
if :math:`H` is :math:`1`-sparse and only contains either real entries or complex ones
then it can be simulated with an error of at most
:math:`\mathcal{O}\left( 2^{-r} \right)` with only 2 black-box oracle calls and
:math:`\mathcal{O}(n+r)` additional quantum gates.

The algorithm to efficiently decompose :math:`H` is described in details in
:cite:`2004-ahokas-graeme-robert-improved-algorithms-for-approximate-quantum-fourier-transforms-and-sparse-hamiltonian-simulations`
and involves graph colouring with only local information.


Approximating :math:`e^{-iH t}` with the :math:`e^{-iH_j t}`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The main method to approximate :math:`e^{-iH t}` is to use the Trotter-Suzuki formula
to decompose the overall exponential into products of simpler exponentials.

The Trotter-Suzuki decomposition of the first order, denoted by :math:`S_2` in
this document, is defined as

.. math::

   S_2(\lambda) = \prod_{j=0}^m e^{H_j\lambda / 2}\prod_{k=m}^0 e^{H_k \lambda / 2}.


The generalisation of this decomposition to an arbitrary order :math:`k` is given
by the recurrence formula

.. math::

   S_{2k}(\lambda) = S_{2k-2}(p_k\lambda)^2 \times S_{2k-2}((1-4p_k)\lambda)
                     \times S_{2k-2}(p_k\lambda)^2

This formula gives a very good approximation of the original matrix. Suzuki proved

.. math::

   \left\vert \left\vert
   e^{\lambda \sum_{j=0}^m H_j} -
   S_{2k}(\lambda)
   \right\vert\right\vert
   \in \mathcal{O}\left( \left\vert \lambda \right\vert^{2k+1} \right)

for :math:`\vert\lambda\vert \to 0`, with :math:`p_k = \left(4 - 4^{1/(2k-1)}\right)^{-1}`.
For our case (Hamiltonian simulation), :math:`\lambda = -it`.

But this formula is only interesting if :math:`t`, the time of the simulation, is small.
In the case that :math:`t` is large, the approximation is not good anymore.

This issue is fixed by simulating :math:`r_{\text{sim}}` times the effect of :math:`H` for
a time :math:`\frac{t}{r_{\text{sim}}}`. This workaround results in the formula

.. math::

   \left\vert \left\vert
   e^{\lambda \sum_{j=0}^m H_j} -
   S_{2k}\left(\frac{\lambda}{r_{\text{sim}}}\right)^{r_{\text{sim}}}
   \right\vert\right\vert
   \in \mathcal{O}\left( \left\vert \frac{\lambda}{r_{\text{sim}}}\right\vert^{2k+1} \right).

Importance of :math:`r_{\text{sim}}`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:math:`r_{\text{sim}}` is one of the most important parameter of the simulation as
it will control the number of repetitions the algorithm will need to perform. A
value for :math:`r_{\text{sim}}` which is :math:`2` times larger than the optimal
value :math:`r_{\text{sim}}^{\text{opt}}` will result in a quantum circuit :math:`2`
times bigger.

Several bounds are currently known, but no tight bound have been found at the moment.

Simulating a single :math:`e^{-iH_j t}`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The simulation of :math:`1`-sparse :math:`H_j` is explained in great details in
:cite:`2004-ahokas-graeme-robert-improved-algorithms-for-approximate-quantum-fourier-transforms-and-sparse-hamiltonian-simulations`.

