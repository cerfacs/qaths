..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.


.. _quantum-walk-algorithm:

❌Quantum walk
==============

Main article: :cite:`0910.4157v4`.

Applicability domain
--------------------

All the :math:`s`-sparse Hamiltonian matrices :math:`H` are simulable with the
quantum-walk algorithm, even if the matrix is not sparse (i.e.
even if :math:`s \not\in \mathcal{O}(\text{poly}(\log N))`), provided that the
following unitary operators are given:

* :math:`O_H`, the black-box acting as

  .. math::

     O_H \vert j,k\rangle \vert z \rangle =
     \vert j,k \rangle \vert z \oplus H_{j,k} \rangle.

* :math:`O_F`, the black-box acting as

  .. math::

     O_F \vert j,k \rangle = \vert j, f(j, k) \rangle,

  where :math:`f(j,k)` is the row (resp. column) index of the :math:`k^\text{th}`
  nonzero element of the column (resp. row) :math:`j`.

Moreover, the algorithm is capable of simulating an arbitrary unitary matrix
provided that there is a black-box :math:`O_U` implementing it:

.. math::

   O_U \vert j,k\rangle \vert z \rangle =
   \vert j,k \rangle \vert z \oplus U_{j,k} \rangle.


Complexity
----------

Let

* :math:`\vert\vert \cdot \vert\vert` be the spectral norm,
* :math:`\vert\vert \cdot \vert\vert_1` be the 1-norm:

  .. math::

     \vert\vert H \vert\vert_1 = \max_j \sum_{k=1}^N \left\vert H_{j,k} \right\vert,

* :math:`\vert\vert \cdot \vert\vert_{+\infty}` be the :math:`\infty`-norm:

  .. math::

     \vert\vert H \vert\vert_{+\infty} = \max_{j,k} \vert H_{j,k}\vert.

Moreover, let :math:`\Lambda_H`, :math:`\Lambda^1_H` and :math:`\Lambda^{max}_H`
be upper-bounds of :math:`\vert\vert H \vert\vert`,
:math:`\vert\vert H \vert\vert_{1}` and :math:`\vert\vert H \vert\vert_{+\infty}`
respectively.


Query complexity
~~~~~~~~~~~~~~~~

For a given Hamiltonian :math:`H`, the evolution under this Hamiltonian for a time
:math:`t` up to an error :math:`\epsilon` can be performed with

.. math::

   \mathcal{O}\left( \frac{\Lambda t}{\sqrt{\epsilon}} + s \Lambda_{max}t \right)

calls to :math:`O_H` and :math:`O_F`.

An other algorithm more suitable for non-sparse Hamiltonians has a query
complexity of

.. math::

   \mathcal{O} \left( s^{2/3} \left[\left(\log \log s \right) \Lambda t\right]^{4/3}
   \epsilon^{-1/3}\right).

Finally, the unitary simulation algorithm needs to call the black-box unitary
:math:`O_U`

.. math::

   \mathcal{O} \left( N^{2/3} \left(\log \log N \right)^{4/3}
   \epsilon^{-1/3}\right)

times.

Gate complexity (excluding oracle queries)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. note::

   The number of quantum gates needed to implement the algorithms is not studied
   in the original article. Maybe other articles will study this?

Available implementations
-------------------------

*No known implementation.*
