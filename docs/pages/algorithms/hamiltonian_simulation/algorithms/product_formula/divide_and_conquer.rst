..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.

.. _divide-conquer-product-formula-algorithm:

❌Divide and conquer method
===========================

Research paper presenting the algorithm: :cite:`Hadfield2018`.

Applicability domain
--------------------

The divide and conquer method presented in the research paper does not specify
a particular method to simulate :math:`H` but rather gives an example of how
it is possible to improve the efficiency of one particular algorithm (the
:ref:`product-formula-algorithm` algorithm) when additional information on the
decomposition is known.

Consequently, the method described in this section is **not** an Hamiltonian
Simulation algorithm but rather a generic method that can be applied to
Hamiltonian Simulation algorithms to lower the number of needed gates.

The applicability domain of this method is the same as the applicability
domain of the Hamiltonian Simulation algorithm used.

The algorithm
-------------

As explained in the previous part, the divide and conquer method presented in
this section is not an Hamiltonian Simulation algorithm by itself: it relies on
another Hamiltonian Simulation algorithm to perform the simulation and is able
to use additional information that may be available to speed-up the used
algorithm.

The research paper studied consider only the case of the
:ref:`product-formula-algorithm` algorithm when the spectral norm of the matrices
:math:`H_i` in the decomposition :math:`H = \sum_i H_i` is known in advance.

The idea comes from the observation that the :ref:`product-formula-algorithm` algorithm
complexity crucially depends on the number of time-step :math:`r`, which in turns
depends on :math:`\Lambda = \max_i ||H_i||`
(see :ref:`product-formula-query-complexity`). This dependence can greatly impact
the performance of the algorithm when only a few of the :math:`H_i` have a large
spectral norm because these few :math:`H_i` will impose to repeat the simulation
of the **whole** Hamiltonian a large number of times.

The idea of the algorithm is then to group the :math:`H_i` with close spectral norms,
simulate each group with a different time-step :math:`r` (depending on the value of
:math:`\Lambda_j = \max_{i \in G_j} ||H_i||` for :math:`j` the index of the considered
group and :math:`G_j` the indices of the :math:`H_i` in group :math:`j`) and then
simulate the whole Hamiltonian by considering the simulated groups (and not each
:math:`H_i`). This comes down to reformulate the decomposition as:

.. math::

   \sum_j \left( \sum_{i \in G_j} H_i \right)

where :math:`G_j` contain the indices of matrices :math:`H_i` that have spectral norms
of the same order of magnitude.

Complexity
----------

The complexity analysis performed in :cite:`Hadfield2018` is quite complex and
long.

*Summarisation still in progress.*

Available implementations
-------------------------

*No known implementation.*
