..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.

Randomised product-formula
==========================

Main article: :cite:`1805.08385v1`.

Applicability domain
--------------------

The Hamiltonian matrix :math:`H` is simulable with the randomised product-formula
algorithm if :math:`H` is simulable with the :ref:`product-formula-algorithm`.

To be simulable with the randomised product-formula, :math:`H` should satisfy:

.. math::

    H = \sum_{j=0}^{m} H_j

with :math:`H_j` an efficiently simulable Hamiltonian for :math:`0 \leqslant j \leqslant m`.

.. admonition:: Example of Hamiltonian :math:`H` that can be simulated with product-formula algorithms:

   #. :ref:`k-local-hamiltonians` with :math:`k \in \mathcal{O}(1)`.
   #. :ref:`sparse-hamiltonians`.


Complexity
----------

The randomisation of the product-formula algorithm improve the number of repetitions
:math:`r^{\text{rand}}_\text{repeat}` needed to obtain the desired accuracy with the drawback that
the guaranteed accuracy is only guaranteed in expectation and no longer guaranteed
unconditionally.

.. warning::

   The following complexity analysis assumes that

   .. math::

      \Lambda = \max_j \vert\vert H_j \vert\vert \in \mathcal{O}(1)

   and seems to make other strong assumptions that may not be verified in
   practice.

   The formulas below may be false or too simple to capture the actual behaviour
   of the algorithm.

Query complexity
~~~~~~~~~~~~~~~~

The query complexity of the randomised algorithm is

.. math::

   \mathcal{O} \left(
   r^{\text{rand}}_{\text{repeat}}\ 5^k m
   \right)


with:

* :math:`r^{\text{rand}}_{\text{repeat}}` the number of time step needed to obtain the desired
  accuracy.

.. math::

   r^{\text{rand}}_{\text{repeat}} = \mathcal{O}\left( mt \max \left\{
      \left( \frac{mt}{\epsilon} \right)^\frac{1}{4k+1},
      \left( \frac{t}{\epsilon} \right)^\frac{1}{2k}
   \right\} \right)

* :math:`k` the order of the Trotter-Suzuki decomposition used.
* :math:`m` the number of terms in the decomposition of :math:`H`. The optimal scaling
  for :math:`m` is :math:`\mathcal{O}(s)`. If no decomposition is given by the user,
  :math:`m \in \mathcal{O}\left( s^2 \right)`.
* :math:`\epsilon` the maximum allowable error for the simulation.

Like in the deterministic algorithm, if only the oracle of :math:`H` is known
then the oracles for the matrices :math:`H_j` can be simulated with
:math:`\mathcal{O}\left(\log^* n\right)` calls to the black-box oracle of
:math:`H`. This gives a query complexity of

.. math::

   \mathcal{O} \left(
   r^{\text{rand}}_{\text{repeat}}\ 5^k m \log^*(n)
   \right).


Gate complexity (excluding oracle queries)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The gate complexity of the randomised algorithm is

.. math::

   \mathcal{O} \left(
   r^{\text{rand}}_{\text{repeat}}\ 5^k m \left( n + r_{\text{prec}} \right)
   \right)


with:

* :math:`r^{\text{rand}}_{\text{repeat}}` the number of time step needed to obtain the desired
  accuracy.

.. math::

   r^{\text{rand}}_{\text{repeat}} = \mathcal{O}\left( mt \max \left\{
      \left( \frac{mt}{\epsilon} \right)^\frac{1}{4k+1},
      \left( \frac{t}{\epsilon} \right)^\frac{1}{2k}
   \right\}\right)

* :math:`k` the order of the Trotter-Suzuki decomposition used.
* :math:`m` the number of terms in the decomposition of :math:`H`. The optimal scaling
  for :math:`m` is :math:`\mathcal{O}(s)`. If no decomposition is given by the user,
  :math:`m \in \mathcal{O}\left( s^2 \right)`.
* :math:`\epsilon` the maximum allowable error for the simulation.
* :math:`n` the number of qubits needed to represent the quantum state :math:`H`
  acts on. :math:`H` is a :math:`2^n \times 2^n` matrix.
* :math:`r_{\text{prec}}` an arbitrary positive integer controlling the precision used to
  represent floating points numbers. See
  :cite:`2004-ahokas-graeme-robert-improved-algorithms-for-approximate-quantum-fourier-transforms-and-sparse-hamiltonian-simulations`
  for the error analysis involving :math:`r_{\text{prec}}` (written as :math:`r` in
  the thesis, the :math:`\text{prec}` has been added in this document to clarify).

Like in the deterministic algorithm, if only the oracle of :math:`H` is known
then the oracles for the matrices :math:`H_j` can be simulated with
:math:`\mathcal{O}\left(n \left(\log^* n\right)^2\right)` additional gates. This
gives a gate complexity of

.. math::

   \mathcal{O} \left(
   r^{\text{rand}}_{\text{repeat}}\ 5^k m \left( n + r_{\text{prec}} \right) n \left( \log^* n \right)^2
   \right).


Available implementations
-------------------------

*No known implementation.*
