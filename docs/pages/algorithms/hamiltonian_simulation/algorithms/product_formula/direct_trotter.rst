..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.

.. _product-formula-algorithm:

Product-formula
===============

Main research paper that introduced the method: :cite:`quant-ph0508139v2`.

Applicability domain
--------------------

The Hamiltonian matrix :math:`H` is simulable with product-formula algorithms if:

.. math::

    H = \sum_{j=0}^{m} H_j

with :math:`H_j` an efficiently simulable Hamiltonian for :math:`0 \leqslant j \leqslant m`.

.. admonition:: Example of Hamiltonian :math:`H` that can be simulated with product-formula algorithms:

   #. :ref:`k-local-hamiltonians` with :math:`k \in \mathcal{O}(1)`.
   #. :ref:`sparse-hamiltonians`.

.. note::

   If the decomposition is not known in advance,
   :cite:`2004-ahokas-graeme-robert-improved-algorithms-for-approximate-quantum-fourier-transforms-and-sparse-hamiltonian-simulations`
   provides an algorithm to compute a decomposition of :math:`H` with a number
   :math:`m = 6d^2 \in \mathcal{O}\left( s^2 \right)` of elementary hamiltonian :math:`H_j`.
   The decomposition computation takes :math:`\mathcal{O}\left( n \left( \log^* n \right) \right)`
   time and requires :math:`\mathcal{O} \left( \log^* n \right)` calls to the
   black-box of :math:`H`.

   Another algorithm explained in :cite:`1003.3683v2` can reduce the number of
   Hamiltonians in the decomposition to :math:`m = 6d \in \mathcal{O}(d)` with the
   drawback that the number of calls to the black-box of :math:`H` increase to
   :math:`\mathcal{O}\left(d + \log^* n\right)`.

Complexity
----------

.. _product-formula-query-complexity:

Query complexity
~~~~~~~~~~~~~~~~

The asymptotic number of black-box call is bounded by the asymptotic number of
simulations of :math:`e^{-iH_j t}` needed to implement :math:`e^{-iH t}`, i.e.

.. math::

   \mathcal{O} \left(
   5^{2k} m^2 \tau \left(\frac{m \tau}{\epsilon}\right)^\frac{1}{2k}
   \right)
   =
   \mathcal{O} \left(
   r^{\text{det}}_{\text{repeat}}\ 5^k m
   \right)


with:

* :math:`r^{\text{det}}_{\text{repeat}}` the number of time step needed to obtain the desired
  accuracy.

.. math::

   r^{\text{det}}_{\text{repeat}} = \mathcal{O}\left( 5^{k} m \tau \left(\frac{m \tau}{\epsilon}\right)^\frac{1}{2k} \right)

* :math:`k` the order of the Trotter-Suzuki decomposition used.
* :math:`m` the number of terms in the decomposition of :math:`H`. The optimal scaling
  for :math:`m` is :math:`\mathcal{O}(s)`. If no decomposition is given by the user,
  :math:`m \in \mathcal{O}\left( s^2 \right)`.
* :math:`\tau = \Lambda t` where :math:`\Lambda = \max_i ||H_i||`.

  .. note::

     In the original paper, the authors used :math:`\Lambda = ||H||` to
     upper-bound :math:`\max_i ||H_i||`. In this complexity analysis, we use the
     best estimate we have, i.e. :math:`\Lambda = \max_i ||H_i||`.

* :math:`\epsilon` the maximum allowable error for the simulation.

But this expression of the query complexity assumes that we have access to a
black-box oracle for each :math:`H_j`. If we do not,
:cite:`2004-ahokas-graeme-robert-improved-algorithms-for-approximate-quantum-fourier-transforms-and-sparse-hamiltonian-simulations`
proved (page 98) that the black-box oracle for :math:`H_j` can be simulated with
:math:`\mathcal{O}\left(\log^* n\right)` calls to the black-box oracle of
:math:`H`. This gives a query complexity of

.. math::

   \mathcal{O} \left(
   r^{\text{det}}_{\text{repeat}}\ 5^k m \log^*(n)
   \right).

Gate complexity (excluding oracle queries)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The gate complexity for generic :ref:`sparse-hamiltonians` is:

.. math::

   \mathcal{O} \left(
   r^{\text{det}}_{\text{repeat}}\ 5^k m \left( n + r_{\text{prec}} \right)
   \right)

with:

* :math:`r^{\text{det}}_{\text{repeat}}` the number of time step needed to obtain the desired
  accuracy.

.. math::

   r^{\text{det}}_{\text{repeat}} = \mathcal{O}\left( 5^{k} m \tau \left(\frac{m \tau}{\epsilon}\right)^\frac{1}{2k} \right)

* :math:`k` the order of the Trotter-Suzuki decomposition used.
* :math:`m` the number of terms in the decomposition of :math:`H`. The optimal scaling
  for :math:`m` is :math:`\mathcal{O}(s)`. If no decomposition is given by the user,
  :math:`m \in \mathcal{O}\left( s^2 \right)`.
* :math:`n` the number of qubits needed to represent the quantum state :math:`H`
  acts on. :math:`H` is a :math:`2^n \times 2^n` matrix.
* :math:`r_{\text{prec}}` an arbitrary positive integer controlling the precision used to
  represent floating points numbers. See
  :cite:`2004-ahokas-graeme-robert-improved-algorithms-for-approximate-quantum-fourier-transforms-and-sparse-hamiltonian-simulations`
  for the error analysis involving :math:`r_{\text{prec}}` (written as :math:`r` in
  the thesis, the :math:`\text{prec}` has been added in this document to clarify).

If only the black-box of :math:`H` is available then following
:cite:`2004-ahokas-graeme-robert-improved-algorithms-for-approximate-quantum-fourier-transforms-and-sparse-hamiltonian-simulations`
it is possible to simulate the oracles :math:`H_j` in
:math:`\mathcal{O}\left( n \left( \log^* n \right)^2 \right)` time. This gives a
complexity of

.. math::

   \mathcal{O} \left(
   r^{\text{det}}_{\text{repeat}}\ 5^k m \left( n + r_{\text{prec}} \right) n \left( \log^* n \right)^2
   \right).


Available implementations
-------------------------

#. **Python 3** - qiskit-acqua (`GitHub <https://github.com/Qiskit/aqua>`__,
   `official website <https://qiskit.org/aqua>`__): `implementation\
   <https://github.com/Qiskit/aqua/blob/master/qiskit_aqua/operator.py#L1315>`__.

   *Restriction*: the implementation only works for Hamiltonians :math:`H` with
   a pre-computed decomposition

   .. math::

      H = \sum_{j=0}^m H_j

   with :math:`H_j` being the tensor product of Pauli operators.
#. **Haskell - Quipper** - simcount (`GitHub <https://github.com/njross/simcount>`__,
   research paper presenting the results: :cite:`1711.10980v1`,
   `presentation completing the research paper\
   <https://www.cs.umd.edu/~amchilds/talks/delft17.pdf>`__):
   `implementation <https://github.com/njross/simcount/blob/master/PF.hs>`__.

   *Restriction*: the implementation has been optimised for the Hamiltonian
   :math:`H` that represents a one-dimensional nearest-neighbor Heisenberg model
   with a random magnetic field in the :math:`z` direction (:math:`h_j` randomly
   chosen in :math:`[-h, h]`):

   .. math::

      H = \sum_{j=1}^n \left( \vec{\sigma_j} \dot{} \vec{\sigma_{j+1}} + h_j \sigma^z_j\right)
