..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.


❌Algorithms based on quantum signal processing
===============================================

Main article: :cite:`1606.02685v2`.

Applicability domain
--------------------

This algorithm is able to simulate :ref:`s-sparse-hamiltonians` provided by an oracle.

Complexity
----------

A :ref:`s-sparse-hamiltonians` :math:`\hat{H}` on :math:`n` qubits with matrix
elements specified to :math:`m` bits of precision can be simulated for time-interval
:math:`t`, error :math:`\epsilon`, and success probability at least :math:`1-2\epsilon`
with

.. math::

   \mathcal{O}\left( ts \vert \vert \hat{H} \vert \vert_{max} +
   \frac{\log \frac{1}{\epsilon}}{\log\log \frac{1}{\epsilon}}\right)

queries and

.. math::

   \mathcal{O} \left( n + m \ \text{poly}\left( \log m \right) \right)

additional quantum gates.

Available implementations
-------------------------

#. **Haskell - Quipper** - simcount (`GitHub <https://github.com/njross/simcount>`__,
   research paper presenting the results: :cite:`1711.10980v1`,
   `presentation completing the research paper\
   <https://www.cs.umd.edu/~amchilds/talks/delft17.pdf>`__):
   `implementation <https://github.com/njross/simcount/blob/master/QSP.hs>`__.

   *Restriction*: the implementation has been optimised for the Hamiltonian
   :math:`H` that represents a one-dimensional nearest-neighbor Heisenberg model
   with a random magnetic field in the :math:`z` direction (:math:`h_j` randomly
   chosen in :math:`[-h, h]`):

   .. math::

      H = \sum_{j=1}^n \left( \vec{\sigma_j} \dot{} \vec{\sigma_{j+1}} + h_j \sigma^z_j\right)

   .. note::

      The implementation may be suitable for more general Hamiltonians but this has
      not been checked for the moment.
