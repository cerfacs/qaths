..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.

❌Algorithms based on "qubitization"
====================================

Main article: :cite:`1610.06546v2`.

Applicability domain
--------------------

Let

* :math:`\hat{U}` be a unitary acting on :math:`q = n + m` qubits,
* :math:`\hat{G}` an oracle such that :math:`\hat{G}\vert 0 \rangle^{\otimes m} = \vert G \rangle`
  with :math:`\vert G \rangle` a :math:`m`-qubit quantum state,
* :math:`\hat{C} = \langle G \vert \hat{U} \vert G \rangle` a complex matrix acting on
  :math:`n` qubits.

Then the matrix :math:`\hat{H} = \frac{1}{2} \left( \hat{C} + \hat{C}^\dagger \right)` is
simulable with the "qubitization" algorithm.

.. note::

   If :math:`\tilde{H} = \langle G \vert \hat{U} \vert G \rangle` is already Hermitian
   then it can be simulated as
   :math:`\tilde{H} = \frac{1}{2} \left( \tilde{H} + \tilde{H}^\dagger \right)`


Complexity
----------

The following complexities applies to the qubitization algorithm when trying to
simulate :math:`e^{-i\hat{H}t}` for a time :math:`t`, an error :math:`\epsilon`
and a failure probability of :math:`\mathcal{O}(\epsilon)`.

Query complexity
~~~~~~~~~~~~~~~~

The algorithm needs

.. math::

   \Theta \left( t + \frac{\log \frac{1}{\epsilon}}{\log \log \frac{1}{\epsilon}} \right)

calls to *controlled*\ -\ :math:`\hat{G}` and *controlled*\ -\ :math:`\hat{U}`.

Gate complexity (excluding oracle queries)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The number of additional single and two-qubit primitive quantum gates needed by the
algorithm is

.. math::

   \mathcal{O} \left( t + \frac{\log \frac{1}{\epsilon}}{\log \log \frac{1}{\epsilon}} \right).

Number of qubits required
~~~~~~~~~~~~~~~~~~~~~~~~~

The algorithm requires :math:`q+2` qubits.

Available implementations
-------------------------

#. **Q#** - Microsoft.Quantum.Canon (`GitHub <https://github.com/Microsoft/QuantumLibraries/tree/master/Canon>`__,
   `official website <https://docs.microsoft.com/en-us/qsharp/api/?view=qsharp-preview>`__): `implementation\
   <https://github.com/Microsoft/QuantumLibraries/blob/master/Canon/src/Simulation/BlockEncoding.qs#L156>`__.
