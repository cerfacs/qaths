# `qaths`

# Contributions are welcomed!

This repository is not maintained on a daily basis anymore and I do not plan to
introduce new features for moment, mainly because the goal I had when developping
QatHS is now reached.

On the other side, I still see several features and ideas that can improve the value
of this repository and would be glad to help if someone want to implement those
features. 

Here is a non-exhaustive list of the features I thought about, sorted by 
expected complexity / required time to implement:

- Implement non-constant propagation speed `c`. 
  - A piece-wise constant `c` should be quite easy to implement (I already 
    implemented a quick proof of concept).
  - More complex functions `c` will probably be harder to implement.
- Implementing other boundary conditions, like Neumann.
- Implementing higher-order Laplacian discretisation. 
- Implementing another Hamultonian Simulation procedure.

If you want to implement one of the ideas above or any idea you have, please 
send me a mail to explain me what is your project. Again, I will be glad
to help you in implementing your idea, and it might end up in a fruitful 
collaboration.

# Installation

Several options are available to install `qaths` on your local machine:

1. Using `pip`.
2. Using `docker`.

Both options are comparable is terms of performance. The `pip` version is preferred for
users that are experienced with python virtual environments (`conda` may be required if
your Python version does not match the required `3.6` version) and for user that will
use regularly or develop in the library. The `docker` option is the preferred 
alternative for users that just want to run the provided command line interfaces without
developing in the library. 

## Cloning the `git` repository

`qaths` is currently only accessible via `git`. Before considering installing it, you 
should retrieve the code from the `repository` with
```shell script
git clone git@gitlab.com:cerfacs/qaths.git
```

## Install with `pip`

In order to install `qaths` with pip you should check that your Python version is
`3.6` with `python --version`. If your Python version is `3.5` or lower or `3.7` or
higher, you will need to have access to a Python `3.6` executable. This can be done
with [`conda`](https://docs.conda.io/en/latest/) by using 
`conda create -n env python=3.6`.

Once Python `3.6` is available, you can install `qaths` with `pip`:

```shell script
# git clone git@gitlab.com:cerfacs/qaths.git  # if not already done
python -m pip install qaths/
``` 

## Install with `docker`

To use `qaths` with the provided `docker` image run:

```shell script
docker login registry.gitlab.com
docker pull registry.gitlab.com/cerfacs/qaths
docker run --rm -it registry.gitlab.com/cerfacs/qaths bash
```


# Replicating the results

All the plots provided in [Practical Quantum Computing: solving the wave equation using
 a quantum approach](https://arxiv.org/abs/2003.12458) can be replicated and the raw 
data used to plot is 100% replicable.

In order to check/replicate the results, install `qaths` following the instructions in 
the [Installation](#installation) section. [Using Docker](#install-with-docker) is the
recommended way.

Once installed, several commands should have been exported into your environment.
Type `qaths.` and try to auto-complete with `TAB` to see the available commands.
All the commands are also listed in the [setup.py](setup.py) file.

If you have issue replicating results, please open an issue on Gitlab describing the 
issue.
